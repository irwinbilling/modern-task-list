package com.irwinbilling.done.data;

public class CheckListItem {

	public String task;
	public boolean complete;
	
	
	public CheckListItem(String taskIn) {
		task = taskIn;
		complete = false;
	}
}
