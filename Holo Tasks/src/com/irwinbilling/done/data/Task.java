package com.irwinbilling.done.data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Task {

	public String title;
	public String notes;
	public long dueDate;
	public long createdDate;
	public List<CheckListItem> checkList;
	public boolean completed;

	public boolean expanded;

	private String mSmallCreatedDate;

	// public Location location;
	
	public Task() {
		title = "";
		notes = "";
		dueDate = DefaultValues.DEFAULT_LONG_NONE;
		createdDate = DefaultValues.DEFAULT_LONG_NONE;
		checkList = new ArrayList<CheckListItem>();
		mSmallCreatedDate = null;
		completed = false;
		expanded = false;
	}
	
	public String getSmallCreatedDate() {
		if (mSmallCreatedDate == null)
			mSmallCreatedDate = milliToSmall(createdDate);
		return mSmallCreatedDate;
	}
	
	private String milliToSmall(long milli) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(milli);
		return c.get(Calendar.DAY_OF_MONTH) + "/" +
			(c.get(Calendar.MONTH) + 1);
	}
}
