package com.irwinbilling.done.data;

import java.util.ArrayList;
import java.util.List;

public class Workspace {

	public String title;
	public WorkspaceColor colors;
	public String bannerUrl;
	
	public List<Task> tasks;
	
	public Workspace() {
		tasks = new ArrayList<Task>();
		title = "";
		colors = new WorkspaceColor();
		bannerUrl = "";
	}
}
