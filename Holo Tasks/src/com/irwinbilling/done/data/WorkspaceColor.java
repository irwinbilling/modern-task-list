package com.irwinbilling.done.data;

public class WorkspaceColor {

	public int startColor;
	public int endColor;
	
	public WorkspaceColor() {
		startColor = 0;
		endColor = 0;
	}
}
