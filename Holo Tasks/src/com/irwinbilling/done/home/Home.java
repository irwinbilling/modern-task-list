package com.irwinbilling.done.home;

import java.util.ArrayList;
import java.util.List;

import android.support.v4.app.FragmentActivity;
import com.irwinbilling.done.R;
import com.irwinbilling.done.data.Task;

import android.os.Bundle;
import android.view.Menu;
import com.irwinbilling.done.tasklist.TaskList;

public class Home extends FragmentActivity {

	private TaskList mTaskList;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		mTaskList = (TaskList) findViewById(R.id.task_list);
		initWorkspaceList();
    }

	@Override
	public void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		mWorkspaceChooser.setWorkspaceListener(this);
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_home, menu);
        return true;
    }
    
    private String createRandString(int len) {
    	StringBuilder builder = new StringBuilder();
    	int start = 'a';
    	int end = 'z';
    	
    	for (int i = 0; i < len; i++) {
    		int gen = (int) ((Math.random() * (end-start)) + start);
    		builder.append((char)gen);
    	}
    	
    	return builder.toString();
    }
    
    private void initCommonProperties() {
    	setSlidingActionBarEnabled(false);
    	mSm.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		mSm.setShadowWidthRes(R.dimen.home_sliding_shadow_size);
    	mSm.setFadeEnabled(false);
    }
    
    private void initWorkspaceChooser() {
    	FrameLayout left = new FrameLayout(this);
    	left.setId("LEFT".hashCode());
    	setBehindLeftContentView(left);

        mWorkspaceChooser = new WorkspaceChooser();
    	
    	getSupportFragmentManager()
    		.beginTransaction()
    		.replace("LEFT".hashCode(), mWorkspaceChooser)
    		.commit();

        initWorkspaceList();

		mSm.setShadowDrawable(R.drawable.shadow, SlidingMenu.LEFT);
		mSm.setBehindOffsetRes(R.dimen.home_sliding_offset, SlidingMenu.LEFT);
    }

    private void initWorkspaceList() {
		int numTasks = (int) (Math.random() * 20);
		List<Task> tasks = new ArrayList<Task>(numTasks);
		for (int j = 0; j < 3; j++) {
			Task t = new Task();

			t.createdDate = System.currentTimeMillis();
			t.dueDate = System.currentTimeMillis();
			t.title = "Test Title " + j;
			t.notes = createRandString((int)(Math.random() * 5));
			tasks.add(t);
		}

		mTaskList.setTasks(tasks);
    }
}
