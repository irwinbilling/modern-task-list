package com.irwinbilling.done.tasklist;

import android.text.Editable;
import android.text.InputType;
import android.text.Spannable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.widget.*;

import android.content.Context;
import android.util.AttributeSet;
import com.irwinbilling.done.R;
import com.irwinbilling.done.data.Task;
import com.irwinbilling.done.data.Workspace;
import com.irwinbilling.done.utils.ExpandAnimation;

import java.util.List;

public class TaskList extends ListView {

	private TaskListAdapter mAdapter;

	public TaskList(Context context) {
		super(context);
		init();
	}

	public TaskList(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public TaskList(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init() {
		mAdapter = new TaskListAdapter(getContext());
		setAdapter(mAdapter);
	}

	public void addTask(Task t) {

	}

	public void setTasks(List<Task> tasks) {
		mAdapter.clear();

		mAdapter.addAll(tasks);
		mAdapter.notifyDataSetChanged();
	}

	private class TaskListAdapter extends ArrayAdapter<Task>
			implements OnClickListener {

		private LayoutInflater mLayoutInflater;

		public TaskListAdapter(Context context) {
			super(context, 0);

			mLayoutInflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;

			if (v == null) {
				v = mLayoutInflater.inflate(R.layout.task_item_layout, null);
				v.setOnClickListener(this);
				setViewHolderTag(v);
			}

			setViewProperties(v, getItem(position));

			return v;
		}

		private void setViewHolderTag(View v) {
			ViewHolder holder = new ViewHolder();

			holder.taskTitle = (TextView) v.findViewById(
					R.id.task_text);
			holder.taskCompleted = (CheckBox) v.findViewById(
					R.id.task_checkbox);
			holder.taskNotes = (TextView) v.findViewById(
					R.id.task_item_details_notes);

			holder.taskHeader = (RelativeLayout) v.findViewById(
					R.id.task_header);
			holder.taskItemDetails = (LinearLayout) v.findViewById(
					R.id.task_item_details);

			v.setTag(holder);
		}

		private void setViewProperties(final View v, final Task t) {
			setAllVisible(v, View.VISIBLE);
			ViewHolder h = (ViewHolder) v.getTag();

			h.taskTitle.setText(t.title);
			h.taskCompleted.setChecked(t.completed);

			h.taskNotes.setText(t.notes);

			// Animate the view if it currently doesn't have the correct
			// orientation.
			int detailsVisibility = h.taskItemDetails.getVisibility();
			if ((t.expanded && detailsVisibility == View.GONE)
					|| (!t.expanded && detailsVisibility == View.VISIBLE)) {
				if (v.getHeight() > 0) {
					runExpandAnimation(v, 200, t);
				} else {
					v.getViewTreeObserver().addOnGlobalLayoutListener(
							new ViewTreeObserver.OnGlobalLayoutListener() {
								@Override
								public void onGlobalLayout() {
									runExpandAnimation(v, 200, t);
									v.getViewTreeObserver()
											.removeGlobalOnLayoutListener(this);
								}
							}
					);
				}
			}
		}

		private void setAllVisible(final View v, final int visible) {
			ViewHolder h = (ViewHolder) v.getTag();

			h.taskTitle.setVisibility(visible);
			h.taskCompleted.setVisibility(visible);
			h.taskNotes.setVisibility(visible);
		}

		@Override
		public void onClick(View v) {
			int position = getPositionForView(v);
			runExpandAnimation(v, 200, getItem(position));
		}

		private void runExpandAnimation(View v, long duration, Task t) {
			ViewHolder h = (ViewHolder) v.getTag();
			if (h.taskItemDetails.getAnimation() != null)
				return;

			// Setting "visible" to be what it will be after the animation.
			t.expanded = h.taskItemDetails.getVisibility() == View.GONE;

			ExpandAnimation anim = new ExpandAnimation(h.taskItemDetails,
					duration);
			anim.setInterpolator(new DecelerateInterpolator());

			h.taskItemDetails.startAnimation(anim);
		}

		private class ViewHolder {
			TextView taskTitle;
			CheckBox taskCompleted;
			TextView taskNotes;

			RelativeLayout taskHeader;
			LinearLayout taskItemDetails;

			ViewHolder() {
			}
		}

		private class Day extends Task {
			String day;
		}

		private abstract class MyTextWatcher implements TextWatcher {

			protected EditText mEditText;

			public MyTextWatcher(EditText editText) {
				mEditText = editText;
			}
		}
	}
}
