package com.irwinbilling.done.tasklist;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.irwinbilling.done.data.Task;
import com.irwinbilling.done.views.TaskView;

public class TaskListAdapter extends BaseAdapter {

	private List<Task> mTasks;
	private Context mContext;
	
	public TaskListAdapter(List<Task> tasks, Context context) {
		mTasks = tasks;
		mContext = context;
	}
	
	@Override
	public int getCount() {
		return mTasks.size();
	}

	@Override
	public Object getItem(int position) {
		return mTasks.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TaskView view;
		
		if (convertView == null) {
			view = new TaskView(mContext);
			
			ViewHolder holder = new ViewHolder();
			
			view.setTag(holder);
		} else {
			view = (TaskView) convertView;
			view.reset();
		}
		
		initializeView((Task) getItem(position), (ViewHolder) view.getTag());
		
		return view;
	}
	
	private void initializeView(Task t, ViewHolder holder) {
	}
	
	private class ViewHolder {
	}
}
