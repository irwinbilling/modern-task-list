package com.irwinbilling.done.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Bitmap.Config;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

/**
 * A view that will fade out as it is scrolled off center until it is fully
 * transparent.
 * 
 * @author Irwin
 *
 */
public class AlphaRelativeLayout extends RelativeLayout {

	private int mAlpha;
	private int mHalfWidth;
	private Bitmap mCachedBitmap;
	private Paint mPaint;
	
	public interface DeletionEventListener {
		public void onDeleteLeft(View v);
		public void onDeleteRight(View v);
		public void onUndo(View v);
	}
	
	public AlphaRelativeLayout(Context context) {
		super(context);
		init(context);
	}
	
	public AlphaRelativeLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	
	public AlphaRelativeLayout(Context context, AttributeSet attrs, 
			int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}
	
	private void init(Context context) {
		mHalfWidth = 100;
		mAlpha = 255;
		mPaint = new Paint();
		mCachedBitmap = null;
		setDrawingCacheEnabled(true);
		setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
	}
	
	public void setContent(View v) {
		if (v == null)
			throw new NullPointerException("View is null");
		
		removeAllViews();
		addView(v);
	}
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
		mHalfWidth = getWidth() / 2;
	}
	
	public void smoothScrollAboveTo(int x) {
	}
	
	@Override
	public void scrollTo(int x, int y) {
		super.scrollTo(x, y);
		
		// Alpha
		int absScroll = Math.abs(getScrollX());
		mAlpha = (int) ((-255.0/mHalfWidth) * absScroll + 255.0);
		if (mAlpha < 0)
			mAlpha = 0;
		
		invalidate();
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		if (mAlpha < 255) {
			if (mCachedBitmap == null) {
				mCachedBitmap = Bitmap.createBitmap(getWidth(), getHeight(), 
						Config.ARGB_8888);
				Canvas c = new Canvas(mCachedBitmap);
				super.onDraw(c);
			}
			
			mPaint.setAlpha(mAlpha);
			canvas.drawBitmap(mCachedBitmap, 0, 0, mPaint);
		} else {
			if (mCachedBitmap != null) {
				// TODO: Might not have to call recycle on new APIs.
				mCachedBitmap.recycle();
				mCachedBitmap = null;
			}
			super.onDraw(canvas);
		}
	}
}
