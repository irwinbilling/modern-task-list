package com.irwinbilling.done.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * A ListView that emulates the awesome bouncing ListView on iOS.
 * 
 * @author Irwin Billing
 *
 */
public class BouncingListView extends ListView {
	
	private int mHeaderHeight;
	private int mHeaderShown;

	public BouncingListView(Context context, AttributeSet attrs, 
			int defStyle) {
		super(context, attrs, defStyle);
	}

	public BouncingListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public BouncingListView(Context context) {
		super(context);
	}
}
