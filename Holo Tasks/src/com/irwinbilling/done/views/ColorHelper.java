package com.irwinbilling.done.views;

/**
 * Copyright 2012, Powered By Technology, Inc.
 */
public class ColorHelper {

	public static final double FACTOR = 0.8;

	/**
	 * Darken an aRGB color.
	 *
	 * @param color	an aRGB color.
	 *
	 * @return	the darkened color.
	 */
	public static int darken(int color) {
		int a = (color >> 24) & 0xFF;
		int r = Math.max((int) (((color >> 16) & 0xFF) * FACTOR), 0);
		int g = Math.max((int) (((color >> 8) & 0xFF) * FACTOR), 0);
		int b = Math.max((int) (((color >> 0) & 0xFF) * FACTOR), 0);

		return ((a & 0xFF) << 24) |
				((r & 0xFF) << 16) |
				((g & 0xFF) << 8)  |
				((b & 0xFF) << 0);
	}
}
