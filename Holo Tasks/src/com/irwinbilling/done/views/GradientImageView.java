package com.irwinbilling.done.views;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Copyright 2012, Powered By Technology, Inc.
 */
public class GradientImageView extends ImageView {

	public static final int DEFAULT_START_COLOR = 0xFFBE1515;
	public static final int DEFAULT_END_COLOR = 0xFF811111;

	public GradientImageView(Context context) {
		super(context);
		init();
	}

	public GradientImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public GradientImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init() {
		setGradient(DEFAULT_START_COLOR,
				ColorHelper.darken(DEFAULT_START_COLOR));
	}

	/**
	 * Set the gradient drawable.
	 *
	 * @param startColor
	 * @param endColor
	 */
	public void setGradient(int startColor, int endColor) {
		int[] colors = {startColor, endColor};
		GradientDrawable drawable = new GradientDrawable(
				GradientDrawable.Orientation.TOP_BOTTOM,
				colors);

		setImageDrawable(drawable);
	}
}
