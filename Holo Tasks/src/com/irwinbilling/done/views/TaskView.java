package com.irwinbilling.done.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

public class TaskView extends View {
	
	private DeletionEventListener mDeleteEventListener;
	private AlphaRelativeLayout mAboveView;
	
	public interface DeletionEventListener {
		public void onDeleteLeft(View v);
		public void onDeleteRight(View v);
		public void onUndo(View v);
	}
	
	public TaskView(Context context) {
		super(context);
		init(context);
	}
	
	public TaskView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	
	public TaskView(Context context, AttributeSet attrs, 
			int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	private void init(Context context) {
	}
	
	public void reset() {
		mAboveView.scrollTo(0, 0);
	}
	
	public void setOnDeletionEventListener(DeletionEventListener l) {
		mDeleteEventListener = l;
	}
}
