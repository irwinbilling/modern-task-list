package com.irwinbilling.done.views;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.graphics.Typeface;

public class TypefaceHelper {
	
	private static Map<String, Typeface> TYPE_FACES = new HashMap<String, Typeface>();
    
    public static Typeface getTypeFace(Context ctx, String font) {
    	if (TYPE_FACES.containsKey(font))
    		return TYPE_FACES.get(font);
    	else {
    		Typeface tf = Typeface.createFromAsset(ctx.getAssets(), font);
    		TYPE_FACES.put(font, tf);
    		return tf;
    	}
    }
}
