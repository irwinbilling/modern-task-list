package com.irwinbilling.done.workspaceview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import android.content.Context;
import android.util.AttributeSet;
import com.irwinbilling.done.data.Task;
import com.irwinbilling.done.data.Workspace;

public class TaskList extends ListView {

	private TaskListAdapter mAdapter;

	public TaskList(Context context) {
		super(context);
		init();
	}

	public TaskList(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public TaskList(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init() {
		mAdapter = new TaskListAdapter(getContext());
	}

	public void setWorkspace(Workspace workspace) {
		mAdapter.clear();
		mAdapter.addAll(workspace.tasks);
	}

	private class TaskListAdapter extends ArrayAdapter<Task> {

		private LayoutInflater mLayoutInflater;

		public TaskListAdapter(Context context) {
			super(context, 0);

			mLayoutInflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = null;

			return v;
		}
	}
}
