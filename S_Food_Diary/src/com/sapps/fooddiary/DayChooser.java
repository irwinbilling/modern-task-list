package com.sapps.fooddiary;

import android.os.Bundle;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.sapps.fooddiary.adapters.DaysListAdapter;
import com.sapps.fooddiary.data.DayFoodList;
import com.sapps.fooddiary.frags.DaysFrag;

/**
 * Copyright 2012, Powered By Technology, Inc.
 */
public class DayChooser extends SherlockFragmentActivity
		implements DaysListAdapter.DaySelectedListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_days_list);
		DaysFrag frag = DaysFrag.newFrag();
        frag.setDaySelectedListener(this);

		getSupportFragmentManager()
				.beginTransaction()
				.replace(R.id.content, frag)
				.commit();
	}

	@Override
	public void dayFoodListSelected(DayFoodList selected) {
	}
}
