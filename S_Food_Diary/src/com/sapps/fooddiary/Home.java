package com.sapps.fooddiary;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.sapps.fooddiary.adapters.FoodItemsAdapter;
import com.sapps.fooddiary.data.Database;
import com.sapps.fooddiary.data.FoodItem;
import com.sapps.fooddiary.utils.Constants;
import com.slidingmenu.lib.SlidingMenu;
import com.sapps.fooddiary.adapters.DaysListAdapter;
import com.sapps.fooddiary.data.DayFoodList;
import com.sapps.fooddiary.frags.DaysFrag;
import com.sapps.fooddiary.frags.FoodListFrag;

import java.util.Calendar;
import java.util.Date;

public class Home extends SherlockFragmentActivity implements
        DaysListAdapter.DaySelectedListener,
        FoodItemsAdapter.OnChangedListener {

	private static final int SUBMENU_CHOOSE_DATE = 200;
	public static final String INTENT_DATE = "intent_date";

    private static final String FRAG_TAG_FOOD_LIST = "frag_foodlist";
    private static final String FRAG_TAG_DAY_LIST = "frag_daylist";
    private static final String HOME_PREFS = "home_prefs";
    private static final String PREF_KEY_FIRST_LAUNCH_FLAG =
            "home_first_launch_flag";

	private long mDate;
	private SlidingMenu mSlidingMenu;
	private FoodListFrag mFoodList;
    private DaysFrag mDaysFrag;
    private boolean mSlidingMenuResponsive;
    private boolean mShowMenuHint;

    /* Variables used to store analytics information. */
    private MixpanelAPI mMixPanel;

	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.responsive_content_frame);

        mMixPanel = MixpanelAPI.getInstance(this,
                Constants.MIXPANEL_TOKEN);

        Intent intent = getIntent();
        mDate = intent.getLongExtra(INTENT_DATE, System.currentTimeMillis());

        SharedPreferences prefs = getSharedPreferences(
                HOME_PREFS, MODE_PRIVATE);
        mShowMenuHint = !prefs.contains(PREF_KEY_FIRST_LAUNCH_FLAG);

		initializeSlidingMenu();

        attachContentFrag();
        attachMenuFrag();
	}

    @Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return mSlidingMenu.onKeyDown(keyCode, event) ||
                    super.onKeyDown(keyCode, event);
	}

    @Override
    protected void onDestroy() {
        mMixPanel.flush();
        super.onDestroy();
    }

    private void initializeSlidingMenu() {
		mSlidingMenu = new SlidingMenu(this);
		mSlidingMenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
		mSlidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        mSlidingMenu.setTouchModeBehind(SlidingMenu.TOUCHMODE_MARGIN);
		mSlidingMenu.setBehindOffsetRes(R.dimen.sliding_menu_behind_offset);
		mSlidingMenu.setShadowWidthRes(R.dimen.sliding_menu_shadow_width);
		mSlidingMenu.setShadowDrawable(R.drawable.sliding_menu_shadow);
        mSlidingMenu.setContentBackground(new ColorDrawable(
                getResources().getColor(R.color.transparent)));
        mSlidingMenu.setBehindScrollScale(0.0f);
		mSlidingMenu.setFadeDegree(.35f);

        // check if the content frame contains the menu frame
        if (findViewById(R.id.menu_frame) == null) {
            mSlidingMenuResponsive = false;
            mSlidingMenu.setMenu(R.layout.menu_frame);
            mSlidingMenu.setMenuBackground(new ColorDrawable(
                    getResources().getColor(R.color.background_side)));
            mSlidingMenu.setSlidingEnabled(true);

            // show home as up so we can toggle
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            // Setup the first-time menu shower.
            if (mShowMenuHint)
                showMenuHintAnimation();
        } else {
            mSlidingMenuResponsive = true;
            // add a dummy view
            View v = new View(this);
            mSlidingMenu.setMenu(v);
            mSlidingMenu.setSlidingEnabled(false);
            mSlidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
        }
	}

    private void showMenuHintAnimation() {
        mSlidingMenu.showMenu();
        final View v = findViewById(R.id.content_frame);
        ViewTreeObserver obs = v.getViewTreeObserver();
        obs.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                setFirstLaunchFlag();
                v.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mSlidingMenu.showContent(true);
                    }
                }, 1000);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                    v.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                else
                    v.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });
    }

    private void setFirstLaunchFlag() {
        SharedPreferences prefs = getSharedPreferences(
                HOME_PREFS, MODE_PRIVATE);
        // The value of the flag is irrelevant, as long as it exists.
        prefs.edit().putBoolean(PREF_KEY_FIRST_LAUNCH_FLAG, true).apply();
    }

    private void attachContentFrag() {
        mFoodList = (FoodListFrag) getSupportFragmentManager()
                .findFragmentByTag(FRAG_TAG_FOOD_LIST);
        if (mFoodList == null) {
            mFoodList = FoodListFrag.newFrag(mDate);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_frame, mFoodList, FRAG_TAG_FOOD_LIST)
                    .commit();
        }
    }

    private void attachMenuFrag() {
        mDaysFrag = (DaysFrag) getSupportFragmentManager()
                .findFragmentByTag(FRAG_TAG_DAY_LIST);
        if (mDaysFrag == null) {
            mDaysFrag = DaysFrag.newFrag();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.menu_frame, mDaysFrag, FRAG_TAG_DAY_LIST)
                    .commit();
        }
        mDaysFrag.setDaySelectedListener(this);
    }

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		menu.addSubMenu(SubMenu.NONE, SUBMENU_CHOOSE_DATE, SubMenu.NONE,
				"Choose Day");
        getSupportMenuInflater().inflate(R.menu.home_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.today:
                setDate(System.currentTimeMillis());
                mMixPanel.track("Button Touched: Today", null);
				break;
			case SUBMENU_CHOOSE_DATE:
                mMixPanel.track("Button Touched: Choose Date", null);
                if (!mSlidingMenuResponsive)
                    mSlidingMenu.showMenu();
				break;
			default:
				break;
		}

		return true;
	}

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                if (!mSlidingMenuResponsive)
                    mSlidingMenu.toggle(true);
                return true;
            default:
                return super.onMenuItemSelected(featureId, item);

        }
    }

    @Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && mSlidingMenu.isMenuShowing()) {
			mSlidingMenu.showContent();
			return true;
		} else {
            return super.onKeyUp(keyCode, event);
        }
	}

    @Override
    protected void onStart() {
        mMixPanel.track("Home Started", null);
        super.onStart();
    }

    private boolean areDatesEqual(long date1, long date2) {
		Calendar date1Cal = Calendar.getInstance();
		Calendar date2Cal = Calendar.getInstance();
		date1Cal.setTime(new Date(date1));
        date2Cal.setTime(new Date(date2));

		return date1Cal.get(Calendar.YEAR) == date2Cal.get(Calendar.YEAR) &&
				date1Cal.get(Calendar.DAY_OF_YEAR) ==  date2Cal.get(
						Calendar.DAY_OF_YEAR);
	}

	@Override
	public void dayFoodListSelected(DayFoodList selected) {
        setDate(selected.getDay());
	}

    private void setDate(long date) {
        if (!areDatesEqual(mDate, date)) {
            mDate = date;
            mFoodList.setDay(date);
            mDaysFrag.setSelectedDay(Database.getInstance(this).get(date));
        }
        mSlidingMenu.showContent();
    }

    @Override
    public void onRemoved(FoodItem item) {
        mDaysFrag.initList();
    }
}
