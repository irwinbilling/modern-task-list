package com.sapps.fooddiary;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.sapps.fooddiary.frags.IntroFrag;
import com.sapps.fooddiary.utils.Constants;

public class Intro extends FragmentActivity {

    public static final String KEY_INTRO_COMPLETE = "intro_complete";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getPreferences(MODE_PRIVATE).getBoolean(
                KEY_INTRO_COMPLETE, false)) {
            Intent intent = new Intent(this, Home.class);
            startActivity(intent);
            finish();
            return;
        }

        setContentView(R.layout.intro_frame);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, new IntroFrag())
                .commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
