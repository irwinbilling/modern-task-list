package com.sapps.fooddiary.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.sapps.fooddiary.R;
import com.sapps.fooddiary.data.DayFoodList;
import com.sapps.fooddiary.frags.DaysFrag;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
* Copyright 2012, Powered By Technology, Inc.
*/
public class DaysListAdapter extends ArrayAdapter<DayFoodList> implements View.OnClickListener {

	private String mCalorieString;
	private DaysFrag daysFrag;
	private View mLastSelectedView;
	private DaySelectedListener mSelectedListener;
    private List<DayFoodList> mObjects;

	public interface DaySelectedListener {
		public void dayFoodListSelected(DayFoodList selected);
	}

	public DaysListAdapter(DaysFrag daysFrag, Context context,
						   List<DayFoodList> list,
						   DaySelectedListener listener, int initialIndex) {
		super(context, 0, list);
        mObjects = list;
		this.daysFrag = daysFrag;
		mCalorieString = daysFrag.getString(R.string.calories_short);
		mSelectedListener = listener;

        DayFoodList selectedList = list.get(initialIndex);
        for (DayFoodList foodList : list)
            foodList.setSelected(foodList == selectedList);
	}

    public List<DayFoodList> getObjects() {
        return mObjects;
    }

    public void setDaySelectedListener(DaySelectedListener listener) {
        mSelectedListener = listener;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		if (convertView == null) {
			convertView = View.inflate(getContext(), R.layout.day_list_item,
					null);
			ListView.LayoutParams params = new ListView.LayoutParams(
					ListView.LayoutParams.MATCH_PARENT,
					daysFrag.getResources().getDimensionPixelSize(
							R.dimen.day_list_item_height));
			convertView.setLayoutParams(params);
			convertView.setBackgroundDrawable(
					getContext().getResources().getDrawable(
							R.drawable.days_list_item_bg));
			holder = new ViewHolder();
			holder.date = (TextView) convertView.findViewById(R.id.day_date);
			holder.calories = (TextView) convertView.findViewById(
					R.id.day_calories);
			holder.selectedIndictaor = convertView.findViewById(
					R.id.selected_indicator);

			holder.selectedIndictaor.setVisibility(View.INVISIBLE);
			convertView.setTag(holder);
			convertView.setOnClickListener(this);
		}

		holder = (ViewHolder) convertView.getTag();
		DayFoodList foodList = getItem(position);

		if (foodList.isSelected()) {
			holder.selectedIndictaor.setVisibility(View.VISIBLE);
			mLastSelectedView = convertView;
		} else {
			holder.selectedIndictaor.setVisibility(View.INVISIBLE);
		}
		holder.dayFoodList = foodList;
		holder.date.setText(getDateString(foodList));
		holder.calories.setText(String.valueOf(
				foodList.getCalories()) + mCalorieString);

		return convertView;
	}

	private String getDateString(DayFoodList foodList) {
		int daysAgo = howManyDaysAgo(foodList.getDay());
		if (daysAgo == 0) {
			return daysFrag.getString(R.string.today);
		} else if (daysAgo == 1) {
			return daysFrag.getString(R.string.yesterday);
		}

		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(foodList.getDay());

		String month = cal.getDisplayName(Calendar.MONTH, Calendar.SHORT,
				Locale.getDefault());
		return month + " " + cal.get(Calendar.DAY_OF_MONTH) + " " +
				cal.get(Calendar.YEAR);
	}

	@Override
	public void onClick(View v) {
		ViewHolder oldHolder = (ViewHolder) mLastSelectedView.getTag();
		oldHolder.dayFoodList.setSelected(false);
		oldHolder.selectedIndictaor.setVisibility(View.INVISIBLE);

		ViewHolder holder = (ViewHolder) v.getTag();
		holder.dayFoodList.setSelected(true);
		holder.selectedIndictaor.setVisibility(View.VISIBLE);
		mLastSelectedView = v;

        if (mSelectedListener != null)
		    mSelectedListener.dayFoodListSelected(holder.dayFoodList);
	}

    public int getSelectedDay() {
        return getPosition(
                ((ViewHolder)mLastSelectedView.getTag()).dayFoodList);
    }

	private int howManyDaysAgo(long date) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(date);
		int day = Calendar.getInstance().get(Calendar.DAY_OF_YEAR) - cal.get(
				Calendar.DAY_OF_YEAR);
		int year = Calendar.getInstance().get(Calendar.YEAR) - cal.get(
				Calendar.YEAR);
		return day + year * 365;
	}

	private class ViewHolder {
		public TextView date;
		public TextView calories;
		public DayFoodList dayFoodList;
		public View selectedIndictaor;
	}
}
