package com.sapps.fooddiary.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.sapps.fooddiary.R;
import com.sapps.fooddiary.data.DayFoodList;
import com.sapps.fooddiary.data.FoodItem;
import com.sapps.fooddiary.frags.RemoveFoodItemFrag;
import com.sapps.fooddiary.utils.bitmap.AsyncBitmapFiller;
import com.sapps.fooddiary.utils.camera.ImageUtils;

import java.io.File;

/**
 * Copyright 2012, Powered By Technology, Inc.
 */
public class FoodItemsAdapter extends ArrayAdapter<FoodItem> {

	public interface OnChangedListener {
		public void onRemoved(FoodItem item);
	}

	private LayoutInflater mInflater;
	private OnChangedListener mListener;
	private DayFoodList mDayFoodList;
    private FragmentManager mFragManager;

	public FoodItemsAdapter(Context context, FragmentManager fragManager,
                            DayFoodList dayFoodList,
                            OnChangedListener listener) {
		super(context, 0);
		mDayFoodList = dayFoodList;
        mFragManager = fragManager;
		mInflater = LayoutInflater.from(context);
		mListener = listener;
	}

	@Override
	public int getCount() {
		return mDayFoodList.getCount();
	}

	@Override
	public FoodItem getItem(int position) {
		return mDayFoodList.getFoodItem(position);
	}

	@Override
	public int getPosition(FoodItem item) {
		return mDayFoodList.getPosition(item);
	}

	@Override
	public void remove(FoodItem object) {
		mDayFoodList.remove(object);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.food_item, null);

			holder = new ViewHolder();
			holder.photoImage = (ImageView) convertView.findViewById(
					R.id.food_item_image);
			holder.infoText = (TextView) convertView.findViewById(
					R.id.food_item_info);

			View removeBtn = convertView.findViewById(R.id.remove_button);
			removeBtn.setTag(holder);
			removeBtn.setOnClickListener(mRemove);

			convertView.setTag(holder);
		}

		holder = (ViewHolder) convertView.getTag();
		holder.foodItem = getItem(position);

		File bmFile = getContext().getFileStreamPath(
				ImageUtils.getImageFileName(holder.foodItem.getId()));
		if (bmFile.exists()) {
			AsyncBitmapFiller.INSTANCE.fillImageView(getContext(),
					Uri.fromFile(bmFile), holder.photoImage);
		} else {
			AsyncBitmapFiller.INSTANCE.fillImageView(getContext(),
					Uri.fromFile(bmFile), holder.photoImage);
		}
		holder.infoText.setText(holder.foodItem.getTimeEaten(getContext()));

		return convertView;
	}

	private class ViewHolder {
		ImageView photoImage;
		TextView infoText;
		FoodItem foodItem;
	}

	private View.OnClickListener mRemove = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			ViewHolder holder = (ViewHolder) v.getTag();
			showRemoveDialog(holder.foodItem);
		}
	};

	private void showRemoveDialog(final FoodItem item) {
        RemoveFoodItemFrag dialog = RemoveFoodItemFrag.newDialog(
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ImageUtils.deleteBitmap(item.getId(), getContext());
                        remove(item);
                        notifyDataSetChanged();
                        mListener.onRemoved(item);
                    }
                });
        dialog.show(mFragManager, "deletefooditem");
	}
}