package com.sapps.fooddiary.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import org.json.JSONException;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class Database {

	private static final String TAG = "Database";

	private static final String SHARED_PREF_NAME_DAYS = "days_list_pref";
	private static final String SHARED_PREF_NAME_META_DATA = "days_list_pref_meta_data";
	private static final String PREF_NAME_MAX_ID = "max_id";

	private static Database mInstance;

	// Map day to food lists.
	private Map<Long, DayFoodList> mDayFoodLists;
	private SharedPreferences mDaysPreferences;
	private SharedPreferences mDaysMetaData;
	private long mMaxId;

	private Database(Context ctx) {
		mDayFoodLists = new HashMap<Long, DayFoodList>();
		try {
			loadPreferences(ctx);
		} catch (JSONException e) {
			Log.e(TAG, "", e);
		}
	}

	public static Database getInstance(Context ctx) {
		if (mInstance == null)
			mInstance = new Database(ctx);

		return mInstance;
	}

	public void save(DayFoodList dayList) {
		Long day = getNormalizedDay(dayList.getDay());
		if (!mDayFoodLists.containsKey(day))
			mDayFoodLists.put(day, dayList);

		try {
			savePreferences();
		} catch (JSONException e) {
			Log.e(TAG, "", e);
		}
	}

	public Collection<DayFoodList> getAllDays() {
		return mDayFoodLists.values();
	}

	/**
	 * Returns null if no day exists.
	 * @param date
	 * @return
	 */
	public DayFoodList get(long date) {
		Long day = getNormalizedDay(date);
		DayFoodList dayList = mDayFoodLists.get(day);
		if (dayList == null) {
			dayList = new DayFoodList(day);
            mDayFoodLists.put(day, dayList);
		}
		return mDayFoodLists.get(day);
	}

	public long generateId() {
		return mMaxId++;
	}

	private Long getNormalizedDay(long time) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(time);

		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		return calendar.getTimeInMillis();
	}

	private void loadPreferences(Context ctx) throws JSONException {
		mDaysPreferences = ctx.getSharedPreferences(SHARED_PREF_NAME_DAYS,
				Context.MODE_PRIVATE);
		mDaysMetaData = ctx.getSharedPreferences(SHARED_PREF_NAME_META_DATA,
				Context.MODE_PRIVATE);

		Map<String, String> vals = (Map<String, String>) mDaysPreferences.getAll();
		for (Map.Entry<String, String> val : vals.entrySet()) {
				mDayFoodLists.put(Long.valueOf(val.getKey()),
						new DayFoodList(val.getValue()));
		}

		mMaxId = mDaysMetaData.getLong(PREF_NAME_MAX_ID, 0);
	}

	private void savePreferences() throws JSONException {
		SharedPreferences.Editor editor = mDaysPreferences.edit();
		SharedPreferences.Editor metaEditor = mDaysMetaData.edit();

		for (Map.Entry<Long, DayFoodList> item : mDayFoodLists.entrySet()) {
			editor.putString(item.getKey().toString(),
					item.getValue().getJson());
		}

		metaEditor.putLong(PREF_NAME_MAX_ID, mMaxId);

		editor.apply();
		metaEditor.apply();
	}
}
