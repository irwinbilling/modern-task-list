package com.sapps.fooddiary.data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Copyright 2012, Powered By Technology, Inc.
 */
public class DayFoodList implements Comparable<DayFoodList> {

	private List<FoodItem> mFoodItems;
	private long mDay;
	private long mCalories;
	private boolean mSelected;

	public DayFoodList(long day) {
		mFoodItems = new ArrayList<FoodItem>();
		mDay = day;
		mSelected = false;
	}

	public DayFoodList(String json) throws JSONException {
		JSONObject obj = new JSONObject(json);
		mDay = obj.getLong("day");

		mFoodItems = new ArrayList<FoodItem>();
		JSONArray foodItems = obj.getJSONArray("foodItems");
		for (int i = 0; i < foodItems.length(); i++) {
			FoodItem item = new FoodItem(foodItems.getJSONObject(i));
			mFoodItems.add(item);
			mCalories += item.getCalories();
		}
		Collections.sort(mFoodItems);
	}

	public boolean isSelected() {
		return mSelected;
	}

	public void setSelected(boolean selected) {
		mSelected = selected;
	}

	public long getDay() {
		return mDay;
	}

	public FoodItem getFoodItem(int position) {
		return mFoodItems.get(position);
	}

	public int getCount() {
		return mFoodItems.size();
	}

	public int getPosition(FoodItem item) {
		return mFoodItems.indexOf(item);
	}

	public long getCalories() {
		return mCalories;
	}

	public void remove(FoodItem item) {
        mCalories -= item.getCalories();
		mFoodItems.remove(item);
	}

	public void addFoodItem(FoodItem item) {
		mFoodItems.add(item);
		mCalories += item.getCalories();
		Collections.sort(mFoodItems);
	}

	public String getJson() throws JSONException {
		JSONObject obj = new JSONObject();

		JSONArray foodItems = new JSONArray();
		for (FoodItem item : mFoodItems) {
			foodItems.put(item.getJsonObject());
		}

		obj.put("day", mDay);
		obj.put("foodItems", foodItems);

		return obj.toString();
	}

	@Override
	public int compareTo(DayFoodList another) {
		if (this.mDay > another.mDay)
			return 1;
		else if (this.mDay == another.mDay)
			return 0;
		else
			return -1;
	}
}
