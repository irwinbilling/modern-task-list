package com.sapps.fooddiary.data;

import android.content.Context;
import android.text.format.DateFormat;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Copyright 2012, Powered By Technology, Inc.
 */
public class FoodItem implements Comparable<FoodItem> {

	private long mTimeEaten;
	private int mCalories;
	private long mId;

	public FoodItem(long id, long timeEaten, int calories) {
		mTimeEaten =  timeEaten;
		mCalories = calories;
		mId = id;
	}

	public FoodItem(JSONObject obj) throws JSONException {
		mTimeEaten = obj.getLong("timeEaten");
		mCalories = obj.getInt("calories");
		mId = obj.getInt("id");
	}

	public String getTimeEaten(Context context) {
		String eaten = mCalories + " calories at " +
				DateFormat.getTimeFormat(context).format(mTimeEaten);
		return eaten;
	}

	public int getCalories() {
		return mCalories;
	}

	public long getId() {
		return mId;
	}

	public JSONObject getJsonObject() throws JSONException {
		JSONObject obj = new JSONObject();
		obj.put("timeEaten", mTimeEaten);
		obj.put("calories", mCalories);
		obj.put("id", mId);
		return obj;
	}

	@Override
	public int compareTo(FoodItem another) {
		if (this.mTimeEaten > another.mTimeEaten)
			return 1;
		else if (this.mTimeEaten == another.mTimeEaten)
			return 0;
		else return -1;
	}
}
