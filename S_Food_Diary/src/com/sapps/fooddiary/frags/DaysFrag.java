package com.sapps.fooddiary.frags;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.sapps.fooddiary.R;
import com.sapps.fooddiary.adapters.DaysListAdapter;
import com.sapps.fooddiary.adapters.DaysListAdapter.DaySelectedListener;
import com.sapps.fooddiary.data.Database;
import com.sapps.fooddiary.data.DayFoodList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Copyright 2012, Powered By Technology, Inc.
 */
public class DaysFrag extends Fragment {

    private static final String KEY_LAST_SELECTED_DAY = "key_last_selected_day";

	private ListView mDaysListView;
	private DaySelectedListener mListener;
    private int mFirstSelectedDay;
    private DaysListAdapter mDaysListAdapter;

	public static DaysFrag newFrag() {
		DaysFrag frag = new DaysFrag();
		return frag;
	}

    public void setDaySelectedListener(DaySelectedListener listener) {
        mListener = listener;
        if (mDaysListAdapter != null)
            mDaysListAdapter.setDaySelectedListener(mListener);
    }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.days_list_layout, container, false);
		mDaysListView = (ListView) v.findViewById(R.id.days_list_view);


        mFirstSelectedDay = 0;
        if (savedInstanceState != null) {
            mFirstSelectedDay = savedInstanceState.getInt(
                    KEY_LAST_SELECTED_DAY);
        }

		return v;
	}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        super.onResume();
        initList();
    }

    public void setSelectedDay(DayFoodList day) {
        List<DayFoodList> days = mDaysListAdapter.getObjects();
        mFirstSelectedDay = days.indexOf(day);
        initList();
    }

    public void initList() {
        // Make sure we have a today.
        Database.getInstance(getActivity()).get(System.currentTimeMillis());

        Collection<DayFoodList> daysCollection = Database.getInstance(
                getActivity()).getAllDays();
        List<DayFoodList> daysList = new ArrayList<DayFoodList>(
                daysCollection.size());

        for (DayFoodList foodList : daysCollection) {
            daysList.add(foodList);
        }

        // Fake days.
//		daysList.add(new DayFoodList(System.currentTimeMillis() - 100000000));
//		daysList.add(new DayFoodList(System.currentTimeMillis() - 200000000));
//		daysList.add(new DayFoodList(System.currentTimeMillis() - 300000000));
//		daysList.add(new DayFoodList(System.currentTimeMillis() - 400000000));

        Collections.sort(daysList);
        Collections.reverse(daysList);

        mDaysListAdapter = new DaysListAdapter(this, getActivity(),
                daysList, mListener, mFirstSelectedDay);
        mDaysListView.setAdapter(mDaysListAdapter);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        mFirstSelectedDay = mDaysListAdapter.getSelectedDay();
        outState.putInt(KEY_LAST_SELECTED_DAY, mFirstSelectedDay);
        super.onSaveInstanceState(outState);
    }
}
