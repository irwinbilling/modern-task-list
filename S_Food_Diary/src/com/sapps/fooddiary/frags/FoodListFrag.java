package com.sapps.fooddiary.frags;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.sapps.fooddiary.R;
import com.sapps.fooddiary.adapters.FoodItemsAdapter;
import com.sapps.fooddiary.data.Database;
import com.sapps.fooddiary.data.DayFoodList;
import com.sapps.fooddiary.data.FoodItem;
import com.sapps.fooddiary.utils.Constants;
import com.sapps.fooddiary.utils.bitmap.BitmapUtils;
import com.sapps.fooddiary.utils.camera.ImageUtils;

import java.io.File;
import java.text.DateFormatSymbols;
import java.util.*;

public class FoodListFrag extends Fragment
		implements FoodItemsAdapter.OnChangedListener {

	private static final String TAG = "FoodListFrag";

	private static final int IMAGE_CAPTURE_ACTION_CODE = 100;

    private class Args {
        static final String DAY = "day";
    }

	private DayFoodList mDayFood;
	private ListView mList;
	private TextView mFoodListTitle;
	private FoodItemsAdapter mAdapter;
	private ImageButton mCameraButton;

    private MixpanelAPI mMixPanel;

	public static FoodListFrag newFrag(long day) {
		FoodListFrag frag = new FoodListFrag();

        Bundle args = new Bundle();
        args.putLong(Args.DAY, day);

        frag.setArguments(args);
		return frag;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.food_items, container, false);

        mMixPanel = MixpanelAPI.getInstance(getActivity(),
                Constants.MIXPANEL_TOKEN);

		mFoodListTitle = (TextView) v.findViewById(R.id.food_list_title);
		mList = (ListView) v.findViewById(R.id.food_list_list);

		mCameraButton = (ImageButton) v.findViewById(
				R.id.camera_launch_button);

		mCameraButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dispatchTakePictureIntent(IMAGE_CAPTURE_ACTION_CODE);
                mMixPanel.track("Camera Opened", null);
			}
		});

        initDay();

		return v;
	}

    private void initDay() {
        setDay(getArguments().getLong(Args.DAY));
    }

    public void setDay(long day) {

		mDayFood = Database.getInstance(getActivity()).get(day);
		updateFoodListTitle();

		// Add a header to the list the exact size as the header for this frag
		// so that the first item in the list view is fully visible when
		// scrolled to the top.
        if (mList.getHeaderViewsCount() == 0) {
            View header = View.inflate(getActivity(),R.layout.food_list_list_header,
                    null);
		    mList.addHeaderView(header);
            header.setVisibility(View.INVISIBLE);
        }

		// Add a footer to the list the exact size as the header for this frag
		// so that the first item in the list view is fully visible when
		// scrolled to the bottom.
        if (mList.getFooterViewsCount() == 0) {
            View footer = View.inflate(getActivity(),
                    R.layout.food_list_list_header, null);
            mList.addFooterView(footer);
            footer.setVisibility(View.INVISIBLE);
        }

		mAdapter = new FoodItemsAdapter(getActivity(),
                getActivity().getSupportFragmentManager(), mDayFood, this);
		mList.setAdapter(mAdapter);

		// Only allow to add new photos if today.
		Calendar listDay = Calendar.getInstance();
		listDay.setTime(new Date(day));
		Calendar now = Calendar.getInstance();
		if (listDay.get(Calendar.YEAR) == now.get(Calendar.YEAR)
				&& listDay.get(Calendar.DAY_OF_YEAR)
				== now.get(Calendar.DAY_OF_YEAR)) {
			mCameraButton.setVisibility(View.VISIBLE);
		} else {
			mCameraButton.setVisibility(View.GONE);
		}
	}

	private void dispatchTakePictureIntent(int actionCode) {
        File outputFile = ImageUtils.getOutputMediaFile();
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(outputFile));
		startActivityForResult(takePictureIntent, actionCode);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode != Activity.RESULT_OK)
			return;

		switch (requestCode) {
			case IMAGE_CAPTURE_ACTION_CODE:
				showNewFoodItemDialog();
				break;
			default:
				break;
		}
	}

	private void showNewFoodItemDialog() {
        mMixPanel.track("Image Captured", null);
		NewFoodItemFrag frag = NewFoodItemFrag.newDialog();
        frag.setOnNewItemListener(
                new NewFoodItemFrag.NewFoodItemFragListener() {
                    @Override
                    public void onNewItem(FoodItem item) {
                        mMixPanel.track("Food Item Created", null);
                        mDayFood.addFoodItem(item);
                        Database.getInstance(getActivity()).save(mDayFood);
                        updateFoodListTitle();
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCanceled() {
                        mMixPanel.track("Food Item Canceled", null);
                    }
                });
		FragmentManager manager = getActivity().getSupportFragmentManager();
		frag.show(manager, "newfooditem");
	}

	private List<FoodItem> getTempItems() {
		List<FoodItem> items = new ArrayList<FoodItem>();

		for (int i = 0; i < 20; i++) {
			FoodItem item = new FoodItem(i, System.currentTimeMillis(), 200);
			items.add(item);
		}

		return items;
	}

	@Override
	public void onRemoved(FoodItem item) {
        mMixPanel.track("Food Item Removed", null);
		Database.getInstance(getActivity()).save(mDayFood);
		updateFoodListTitle();
        ((FoodItemsAdapter.OnChangedListener)getActivity()).onRemoved(item);
	}

	private void updateFoodListTitle() {
		StringBuilder text = new StringBuilder();
		int dayDiff = howManyDaysAgo(new Date(mDayFood.getDay()));
		if (dayDiff == 0) {
			text.append(getString(R.string.today_caps));
		} else if (dayDiff == 1){
			text.append(getString(R.string.yesterday_caps));
		} else {
			text.append(getDateString(new Date(mDayFood.getDay())));
		}

		text.append(": ");
		text.append(mDayFood.getCalories());
		text.append(" ");
		text.append(getString(R.string.calories));

		mFoodListTitle.setText(text.toString());
	}

	private String getDateString(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		String month = DateFormatSymbols.getInstance(Locale.getDefault())
				.getMonths()[cal.get(Calendar.MONTH)].substring(0, 3);

		StringBuilder builder = new StringBuilder();
		builder.append(month);
		builder.append(" ");
		builder.append(cal.get(Calendar.DAY_OF_MONTH));
		builder.append(" ");
		builder.append(cal.get(Calendar.YEAR));

		return builder.toString();
	}

	private int howManyDaysAgo(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int day = Calendar.getInstance().get(Calendar.DAY_OF_YEAR) - cal.get(
				Calendar.DAY_OF_YEAR);
		int year = Calendar.getInstance().get(Calendar.YEAR) - cal.get(
				Calendar.YEAR);
		return day + year * 365;
	}
}
