package com.sapps.fooddiary.frags;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.sapps.fooddiary.Home;
import com.sapps.fooddiary.Intro;
import com.sapps.fooddiary.R;
import com.sapps.fooddiary.utils.Constants;
import com.viewpagerindicator.CirclePageIndicator;
import org.json.JSONException;
import org.json.JSONObject;


public class IntroFrag extends Fragment {

    private static final int INTRO_SIZE = 4;

    private Button mPrevious;
    private Button mNext;
    private Button mSkipIntro;
    private ViewPager mViewPager;

    /* Variables used to store analytics information. */
    private MixpanelAPI mMixPanel;
    private int mFarthestIntroPage;
    private boolean mHitSkip;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFarthestIntroPage = 0;
        mHitSkip = false;
        mMixPanel = MixpanelAPI.getInstance(getActivity(),
                Constants.MIXPANEL_TOKEN);

        View v = inflater.inflate(R.layout.intro_layout, container, false);

        mViewPager = (ViewPager) v.findViewById(R.id.view_pager);
        mViewPager.setAdapter(new IntroPagerAdapter(getActivity()));

        CirclePageIndicator indicator = (CirclePageIndicator) v.findViewById(
                R.id.view_pager_indicator);
        indicator.setViewPager(mViewPager);
        indicator.setOnPageChangeListener(onPageChanged);

        mPrevious = (Button) v.findViewById(R.id.previous);
        mPrevious.setOnClickListener(mOnPrev);

        mNext = (Button) v.findViewById(R.id.next);
        mNext.setOnClickListener(mOnNext);

        mSkipIntro = (Button) v.findViewById(R.id.skip);
        mSkipIntro.setOnClickListener(mOnSkip);

        updateButtons();

        return v;
    }

    @Override
    public void onStart() {
        mMixPanel.track("Intro Started", null);
        super.onStart();
    }

    private void updateButtons() {
        mNext.setEnabled(mViewPager.getCurrentItem() < INTRO_SIZE - 1);
        mPrevious.setEnabled(mViewPager.getCurrentItem() > 0);

        if (mViewPager.getCurrentItem() == INTRO_SIZE - 1 &&
                mSkipIntro.getAnimation() == null) {
            AlphaAnimation anim = new AlphaAnimation(1, 0);
            anim.setDuration(500);
            anim.setFillAfter(true);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {}

                @Override
                public void onAnimationEnd(Animation animation) {
                    mSkipIntro.setVisibility(View.INVISIBLE);
                    mSkipIntro.setEnabled(false);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            mSkipIntro.startAnimation(anim);
        }
    }

    private ViewPager.OnPageChangeListener onPageChanged =
            new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i2) {}

        @Override
        public void onPageSelected(int i) {
            if (!mHitSkip && i > mFarthestIntroPage)
                mFarthestIntroPage = i;

            updateButtons();
        }

        @Override
        public void onPageScrollStateChanged(int i) {}
    };

    private View.OnClickListener mOnGetStarted = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SharedPreferences prefs = getActivity().getPreferences(
                    Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(Intro.KEY_INTRO_COMPLETE, true);
            editor.commit();

            mMixPanel.track("Intro Completed", null);

            Intent intent = new Intent(getActivity(), Home.class);
            getActivity().startActivity(intent);

            getActivity().finish();
        }
    };

    private View.OnClickListener mOnSkip = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            JSONObject props = new JSONObject();
            try {
                props.put("Furthest Intro Page Reached", mFarthestIntroPage);
            } catch (JSONException e) {}

            mHitSkip = true;
            mMixPanel.track("Intro Skipped", props);
            mViewPager.setCurrentItem(mViewPager.getAdapter().getCount() - 1,
                    true);
        }
    };

    private View.OnClickListener mOnNext = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1, true);
            updateButtons();
        }
    };

    private View.OnClickListener mOnPrev = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1, true);
            updateButtons();
        }
    };

    private class IntroPagerAdapter extends PagerAdapter {

        private LayoutInflater mInflater;

        public IntroPagerAdapter(Context context) {
            super();
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return INTRO_SIZE;
        }

        @Override
        public void destroyItem(ViewGroup container, int position,
                                Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object o) {
            return view == o;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View v;
            if (position == 0)
                v = mInflater.inflate(R.layout.intro_page_1, null, false);
            else if (position == 1)
                v = mInflater.inflate(R.layout.intro_page_2,  null, false);
            else if (position == 2)
                v = mInflater.inflate(R.layout.intro_page_3, null, false);
            else if (position == 3) {
                v = mInflater.inflate(R.layout.intro_page_4, null, false);
                v.findViewById(R.id.lets_get_started).setOnClickListener(
                        mOnGetStarted);
            }
            else
                v = null;

            container.addView(v);

            return v;
        }
    }
}
