package com.sapps.fooddiary.frags;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.sapps.fooddiary.R;
import com.sapps.fooddiary.data.Database;
import com.sapps.fooddiary.data.FoodItem;
import com.sapps.fooddiary.utils.camera.ImageUtils;

/**
 * Copyright 2012, Powered By Technology, Inc.
 */
public class NewFoodItemFrag extends DialogFragment {

	private static final String TAG = NewFoodItemFrag.class.getSimpleName();

	public interface NewFoodItemFragListener {
		public void onNewItem(FoodItem item);
        public void onCanceled();
	}

	private EditText mCalories;
	private NewFoodItemFragListener mListener;

	public static NewFoodItemFrag newDialog() {
		NewFoodItemFrag frag = new NewFoodItemFrag();

		return frag;
	}

    public void setOnNewItemListener(NewFoodItemFragListener listener) {
        mListener = listener;
    }

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View layout = inflater.inflate(R.layout.food_item_new, null);
		mCalories = (EditText) layout.findViewById(R.id.number_of_calories);

		AlertDialog.Builder builder = new AlertDialog.Builder(
                new ContextThemeWrapper(getActivity(), R.style.AppThemeDialog));

		builder.setView(layout)
				.setPositiveButton(R.string.add,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
												int which) {
								try {
									int val = Integer.valueOf(
											mCalories.getText().toString());
									saveFoodItem(val);
								} catch (NumberFormatException e) {
									Toast.makeText(getActivity(),
											"Please enter only numbers.",
											Toast.LENGTH_LONG).show();
								}
							}
						})
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        mListener.onCanceled();
                    }
                });

		return builder.create();
	}

	private void saveFoodItem(int calories) {
		long id = Database.getInstance(getActivity())
				.generateId();
        saveBitmap(id);
		FoodItem newItem = new FoodItem(id,
				System.currentTimeMillis(), calories);
		mListener.onNewItem(newItem);
	}

	private void saveBitmap(final long id) {
		ImageUtils.saveTempBitmap(getActivity(), id);
	}
}
