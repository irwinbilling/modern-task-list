package com.sapps.fooddiary.frags;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import com.sapps.fooddiary.R;


public class RemoveFoodItemFrag extends DialogFragment {

    private static final String TAG = RemoveFoodItemFrag.class.getSimpleName();

    private DialogInterface.OnClickListener mListener;

    public static RemoveFoodItemFrag newDialog(DialogInterface.OnClickListener
                                                    listener) {
        RemoveFoodItemFrag frag = new RemoveFoodItemFrag();
        frag.mListener = listener;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layout = inflater.inflate(R.layout.food_item_remove, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(
                new ContextThemeWrapper(getActivity(), R.style.AppThemeDialog));

        builder.setView(layout)
                .setPositiveButton(R.string.remove, mListener);
        return builder.create();
    }
}
