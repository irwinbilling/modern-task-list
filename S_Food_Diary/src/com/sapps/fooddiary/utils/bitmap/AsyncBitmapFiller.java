package com.sapps.fooddiary.utils.bitmap;

import android.content.Context;
import android.net.Uri;
import android.widget.ImageView;

import java.net.URL;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Fill bitmaps asynchronously, using a thread pool.
 *
 * Copyright 2012, Powered By Technology, Inc.
 */
public enum AsyncBitmapFiller {
	INSTANCE;

	private static class Constants {
		private static final int KEEP_ALIVE_TIME = 1;

		private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;

		private static int NUMBER_OF_CORES =
				Runtime.getRuntime().availableProcessors();
	}

	private BlockingQueue<Runnable> mWorkQueue;
	private ThreadPoolExecutor mThreadPool;

	private AsyncBitmapFiller() {
		mWorkQueue = new LinkedBlockingQueue<Runnable>();
		mThreadPool = new ThreadPoolExecutor(
				Constants.NUMBER_OF_CORES,
				Constants.NUMBER_OF_CORES,
				Constants.KEEP_ALIVE_TIME,
				Constants.KEEP_ALIVE_TIME_UNIT,
				mWorkQueue);
	}

	public BitmapFillerRunnable fillImageView(Context ctx, int bitmapResourceId,
							  ImageView imageView) {
		BitmapFillerRunnable runnable = new BitmapFillerRunnable(ctx, imageView,
				bitmapResourceId);
		mThreadPool.execute(runnable);
		return runnable;
	}

	public BitmapFillerRunnable fillImageView(Context ctx, Uri uri,
							  ImageView imageView) {
		BitmapFillerRunnable runnable = new BitmapFillerRunnable(ctx, imageView,
				uri);
		mThreadPool.execute(runnable);
		return runnable;
	}

	public BitmapFillerRunnable fillImageView(Context ctx, URL url,
							  ImageView imageView) {
		BitmapFillerRunnable runnable = new BitmapFillerRunnable(ctx, imageView,
				url);
		mThreadPool.execute(runnable);
		return runnable;
	}
}
