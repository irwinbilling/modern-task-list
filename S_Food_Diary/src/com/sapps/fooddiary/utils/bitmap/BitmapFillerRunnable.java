package com.sapps.fooddiary.utils.bitmap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * If a URL is used, the bitmap will be cached.
 * Copyright 2012, Powered By Technology, Inc.
 */
public class BitmapFillerRunnable implements Runnable {

	private static final String TAG = BitmapFillerRunnable.class
			.getSimpleName();

	private Context mContext;
	private URL mUrl;
	private Uri mUri;
	private int mBitmapResourceId;
	private ImageView mImageView;

	public BitmapFillerRunnable(Context ctx, ImageView imageView, URL url) {
		mContext = ctx;
		mUrl = url;
		mImageView = imageView;
	}

	public BitmapFillerRunnable(Context ctx, ImageView imageView, Uri uri) {
		mContext = ctx;
		mUri = uri;
		mImageView = imageView;
	}

	public BitmapFillerRunnable(Context ctx, ImageView imageView,
								int bitmapResourceId) {
		mContext = ctx;
		mBitmapResourceId = bitmapResourceId;
		mImageView = imageView;
	}

	public void clearImageview() {
		synchronized (mImageView) {
			mImageView = null;
		}
	}

	@Override
	public void run() {
		if (mUrl != null) {
		} else if (mUri != null) {
			try {
                // Initialize the imageview to a color.
                mImageView.post(new Runnable() {
                    @Override
                    public void run() {
                        mImageView.setImageDrawable(new ColorDrawable());
                    }
                });
				fillImageView(mUri);
			} catch (FileNotFoundException e) {
				Log.e(TAG, "Could not load image: "
						+ mUri.toString(), e);
			}
		} else if (mBitmapResourceId != 0) {
			fillImageView(mContext.getResources().openRawResource(
					mBitmapResourceId));
		}
	}

	private void fillImageView(Uri uri) throws FileNotFoundException {
		final BitmapFactory.Options options = BitmapUtils.createDecodeOptions();

        InputStream is = mContext.getContentResolver().openInputStream(
                uri);

		BitmapFactory.decodeStream(is, null, options);

		synchronized (mImageView) {
			if (mImageView != null) {
				BitmapUtils.modifyOptionsForLoading(mContext, options,
						mImageView);
			} else {
				return;
			}
		}

		try {
            try {
                is.close();
            } catch (IOException e) {}

            is = mContext.getContentResolver().openInputStream(
                    uri);
			final Bitmap bm = BitmapFactory.decodeStream(is, null, options);

			Handler handler = new Handler(Looper.getMainLooper());
			handler.post(new Runnable() {
				@Override
				public void run() {
					fillImageView(bm);
				}
			});
		} catch(IOException e) {
			Log.e(TAG, "Problem loading stream", e);
		} finally {
			try {
				is.close();
			} catch (IOException e) {}
		}
	}

    private void fillImageView(InputStream is)  {
        final BitmapFactory.Options options = BitmapUtils.createDecodeOptions();

        // We need reset to work :).
        if (!(is instanceof BufferedInputStream)) {
            is = new BufferedInputStream(is);
        }

        BitmapFactory.decodeStream(is, null, options);

        synchronized (mImageView) {
            if (mImageView != null) {
                BitmapUtils.modifyOptionsForLoading(mContext, options,
                        mImageView);
            } else {
                return;
            }
        }

        try {
            is.reset();
            final Bitmap bm = BitmapFactory.decodeStream(is, null, options);

            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    fillImageView(bm);
                }
            });
        } catch(IOException e) {
            Log.e(TAG, "Problem loading stream", e);
        } finally {
            try {
                is.close();
            } catch (IOException e) {}
        }
    }

	private void fillImageView(Bitmap bm) {
		synchronized (mImageView) {
			if (mImageView != null) {
				mImageView.setImageBitmap(bm);
                AlphaAnimation anim = new AlphaAnimation(0, 1);
                anim.setDuration(300);
                mImageView.startAnimation(anim);
				mImageView = null;
			}
		}
	}
}
