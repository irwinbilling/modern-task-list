package com.sapps.fooddiary.utils.bitmap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class BitmapUtils {

	private static final String TAG = BitmapUtils.class.getSimpleName();
	private static final String CLOSE_CDN = "http://i.media.dev.corp.close.com";

	/**
	 * Load a bitmap at the required resolution to fill the ImageView.  No
	 * smaller, no larger.  This method uses imgView.layoutParams.width/height.
	 *
	 * Method is blocking.
	 *
	 * @param bitmapResourceId	the bitmap to load.
	 * @param imgView			the view to fill.
	 */
	public static void fillImageViewEfficient(Context ctx,
											  int bitmapResourceId,
											  ImageView imgView)
			throws IOException {
		fillImageViewEfficient(ctx, ctx.getResources().openRawResource(
			bitmapResourceId),
			imgView);
	}

	/**
	 * @see com.sapps.fooddiary.utils.bitmap.BitmapUtils.fillImageViewEfficient( android.content.res.Resources, int, android.widget.ImageView);
	 *
	 * Method is blocking.
	 *
	 * @param ctx
	 * @param uri
	 * @param imgView
	 */
	public static void fillImageViewEfficient(Context ctx,
												Uri uri,
												ImageView imgView)
			throws IOException {

		// Wrap in buffered stream because fillImageViewEfficient requires
		// reset().
		InputStream stream = new BufferedInputStream(
				ctx.getContentResolver().openInputStream(uri));

		fillImageViewEfficient(ctx, stream, imgView);
	}

	/**
	 * @see com.sapps.fooddiary.utils.bitmap.BitmapUtils.fillImageViewEfficient( android.content.res.Resources, int, android.widget.ImageView);
	 *
	 *
	 * @param is
	 * @param imgView
	 * @param ctx
	 *
	 * @throws java.io.IOException
	 */
	public static void fillImageViewEfficient(Context ctx, final InputStream is,
												final ImageView imgView)
			throws IOException {
		final BitmapFactory.Options options = createDecodeOptions();

		BitmapFactory.decodeStream(is, null, options);

		modifyOptionsForLoading(ctx, options, imgView);

		is.reset();

		Handler handler = new Handler(Looper.getMainLooper());
		handler.post( new Runnable() {
			@Override
			public void run() {
				Bitmap bm = BitmapFactory.decodeStream(is, null, options);
				imgView.setImageBitmap(bm);
			}
		});
	}

	public static BitmapFactory.Options createDecodeOptions() {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		return options;
	}

	public static void modifyOptionsForLoading(Context ctx,
											   BitmapFactory.Options options,
											   ImageView imgView) {
		float density = ctx.getResources().getDisplayMetrics().density;
		int width = (int) (imgView.getLayoutParams().width * density);
		int height = (int) (imgView.getLayoutParams().height * density);

        if (width < 0)
            width = height;

		options.inSampleSize = calculateInSampleSize(options, width, height);
		options.inJustDecodeBounds = false;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
	}

	public static int calculateInSampleSize(
			BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and
			// width
			final int heightRatio = Math.round(
					(float) height / (float) reqHeight);
			final int widthRatio = Math.round(
					(float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee a final image with both dimensions larger than or equal
			// to the requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}
	
	public static URL getCloseImageUrl(String imageId)
			throws MalformedURLException {
		if (imageId.length() == 0)
			throw new MalformedURLException("Invalid imageId");
		StringBuilder url = new StringBuilder();
		
		url.append(CLOSE_CDN);
		for (int i = 0; i < 3; i++) {
			url.append('/');
			url.append(imageId.charAt(i));
		}
		url.append('/');
		url.append(imageId);
		url.append(".jpg");

		return new URL(url.toString());
	}
}
