package com.sapps.fooddiary.utils.bitmap;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

/**
 * Copyright 2012, Powered By Technology, Inc.
 */
public class UrlBitmapFileCache {

	private static final String TAG = UrlBitmapFileCache.class.getSimpleName();
	private static final Bitmap.CompressFormat COMPRESS_FORMAT =
			Bitmap.CompressFormat.JPEG;
	private static final int COMPRESS_QUALITY = 100;

	/**
	 * Open a bitmap.  Return null if it does not exist.
	 *
	 * @param ctx
	 * @param url
	 *
	 * @return	the stream to read the bitmap from, null if not found.
	 */
	public static FileInputStream openBitmap(Context ctx, URL url) {
		String filename = urlToFilename(url);
		try {
			return ctx.openFileInput(filename);
		} catch (FileNotFoundException e) { /* File does not exist :o */ }

		return null;
	}

	/**
	 * If saving failed, this method fails silently.  It does however log
	 * and report to Bug Sense.
	 *
	 * @param ctx
	 * @param bitmap
	 * @param url
	 */
	public static void saveBitmap(Context ctx, Bitmap bitmap, URL url) {
		String filename = urlToFilename(url);
		try {
			FileOutputStream stream = ctx.openFileOutput(
					filename, Context.MODE_PRIVATE);
			bitmap.compress(COMPRESS_FORMAT,COMPRESS_QUALITY, stream);
			stream.close();
		} catch (FileNotFoundException e) {
			Log.e(TAG, "Unable to write to private storage.", e);
		} catch (IOException e) {
			Log.e(TAG, "Unable to write to private storage.", e);
		}
	}

	/**
	 * Create a filename from the URL string.
	 *
	 * @param url
	 *
	 * @return	A string taking the form: [url.hashCode()].[extension]
	 */
	public static String urlToFilename(URL url) {
		String file = url.getFile();
		return url.hashCode() + file.substring(file.lastIndexOf('.'));
	}
}
