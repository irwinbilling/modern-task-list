package com.sapps.fooddiary.utils.camera;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import com.sapps.fooddiary.utils.bitmap.BitmapUtils;

import java.io.*;

/**
 * Copyright 2012, Powered By Technology, Inc.
 */
public class ImageUtils {

	private static final String TAG = ImageUtils.class.getSimpleName();

    private static final String TEMP_TILE_NAME = "temp";
    private static final String IMG_EXTENSION =
            Bitmap.CompressFormat.JPEG.toString();

    private static final int IMG_WIDTH = 512;
    private static final int IMG_HEIGHT = 512;

	/** Create a File for saving an image or video */
	public static File getOutputMediaFile(){
		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.

		File mediaStorageDir = new File(
				Environment.getExternalStoragePublicDirectory(
						Environment.DIRECTORY_PICTURES), "com.sapps.fooddiary");
		// This location works best if you want the created images to be shared
		// between applications and persist after your app has been uninstalled.

		// Create the storage directory if it does not exist
		if (! mediaStorageDir.exists()){
			if (! mediaStorageDir.mkdirs()){
				Log.d("com.sapps.fooddiary", "failed to create directory");
				return null;
			}
		}

		// Create a media file name
		return new File(mediaStorageDir.getPath() + File.separator +
                TEMP_TILE_NAME + "." + IMG_EXTENSION);
	}

	public static String getImageFileName(long id) {
		return id + "_img." + IMG_EXTENSION;
	}

	public static void saveTempBitmap(final Context ctx,
                                      final long id) {
        BitmapFactory.Options options = BitmapUtils.createDecodeOptions();
        options.inSampleSize = BitmapUtils.calculateInSampleSize(options,
                IMG_WIDTH, IMG_HEIGHT);
        options.inJustDecodeBounds = false;
        options.inPreferQualityOverSpeed = true;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        File tempFile = getOutputMediaFile();
        String destination = getImageFileName(id);
        File out = new File(ctx.getFilesDir(), destination);

        Bitmap src = BitmapFactory.decodeFile(tempFile.getAbsolutePath(),
                options);
        try {
            copy(src, out);
        } catch (IOException e) {
            Log.e(TAG, "Error moving temp file!");
        } finally {
            if (src != null)
                src.recycle();
            if (tempFile.delete())
                Log.e(TAG, "Error deleting temp file!");
        }
    }

    private static void copy(Bitmap src, File dst) throws IOException {
        OutputStream out = new FileOutputStream(dst);
        src.compress(Bitmap.CompressFormat.JPEG, 90, out);
    }

	public static void deleteBitmap(long id, Context ctx) {
		String file = getImageFileName(id);
		ctx.deleteFile(file);
	}
}
