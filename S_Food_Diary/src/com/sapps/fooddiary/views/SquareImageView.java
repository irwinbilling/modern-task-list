package com.sapps.fooddiary.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Copyright 2012, Powered By Technology, Inc.
 */
public class SquareImageView extends ImageView {

	public SquareImageView(Context context) {
		super(context);
	}

	public SquareImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public SquareImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int maxLength = Math.max(widthMeasureSpec, heightMeasureSpec);
		super.onMeasure(maxLength, maxLength);
	}
}
